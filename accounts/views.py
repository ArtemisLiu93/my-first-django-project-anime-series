from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from accounts.forms import RegisterForm, Login_form
from django.contrib.auth import login, authenticate, logout

# Create your views here.

def registration_view(response):

    form = RegisterForm()
    if response.method=="POST":
        form = RegisterForm(response.POST)
        if form.is_valid():
            user = form.save()
            login(response, user)
            return redirect('home')

    context = {
        'form': form
    }

    return render(response, 'accounts/signup.html', context)

def login_view(request):
    if request.method == "POST":
        form = Login_form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = Login_form()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return redirect('login')


