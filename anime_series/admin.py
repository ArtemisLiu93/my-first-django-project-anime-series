from django.contrib import admin
from .models import AnimeSerie, Character, Character_detail

# Register your models here.
@admin.register(AnimeSerie)
class AnimeSerieAdmin(admin.ModelAdmin):
    list_display= (
        "name",
        "image",
        "description",
    )

@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    list_display= (
        "name",
        "image",
        "description",
    
    )

@admin.register(Character_detail)
class Character_detailAdmin(admin.ModelAdmin):
    list_display= (
        "info",
        "image1",
    )

     
