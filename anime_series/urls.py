from anime_series.views import anime_list, anime_individual, character_info, create_character_form
from django.urls import path


urlpatterns = [
    path("", anime_list, name="home"),
    path("anime/<int:id>/", anime_individual, name="anime-individual"),
    path("character_info/<int:id>/", character_info, name= "character_info"),
    path("character/create/", create_character_form, name= "create_character_form"),
]

