# Generated by Django 4.2 on 2023-04-27 01:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("anime_series", "0005_rename_character_id_character_anime_id"),
    ]

    operations = [
        migrations.CreateModel(
            name="Character_details",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("image1", models.URLField()),
                ("image2", models.URLField()),
                ("image3", models.URLField()),
                ("image4", models.URLField()),
                ("image5", models.URLField()),
                ("info", models.CharField(max_length=1000)),
                (
                    "character_id",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="details",
                        to="anime_series.character",
                    ),
                ),
            ],
        ),
    ]
