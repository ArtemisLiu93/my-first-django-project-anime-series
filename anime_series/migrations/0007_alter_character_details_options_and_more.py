# Generated by Django 4.2 on 2023-04-27 02:16

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("anime_series", "0006_character_details"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="character_details",
            options={"ordering": ["order"]},
        ),
        migrations.AddField(
            model_name="character_details",
            name="order",
            field=models.PositiveIntegerField(null=True),
        ),
    ]
