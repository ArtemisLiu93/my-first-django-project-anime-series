from django import forms 


class Character_creation_form(forms.Form):
    name= forms.CharField(max_length=200)
    image= forms.URLField()
    description = forms.CharField(max_length=800)
    anime_id = forms.IntegerField(max_value=500)