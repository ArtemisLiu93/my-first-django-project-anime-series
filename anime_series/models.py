from django.db import models





# Create your models here.
#! anime series
class AnimeSerie(models.Model):
    name= models.CharField(max_length=200)
    image= models.URLField()
    description= models.CharField(max_length=1000, null=True)
    # characers = models.ManyToManyField('anime_series.Character')

    def __str__(self):
        return self.name

#! characters
class Character(models.Model):
    name= models.CharField(max_length=200)
    image= models.URLField()
    description = models.CharField(max_length=800)

    order= models.PositiveIntegerField()
    anime_id= models.ForeignKey(
        AnimeSerie,
        related_name= "Character",
        on_delete=models.CASCADE
    )

    # user= models.ForeignKey(
    #     settings.AUTH_USER_MODEL,
    #     related_name= "characters",
    #     on_delete= models.CASCADE,
    # )

    class Meta:
        ordering= ["order"]

#! character information
class Character_detail(models.Model):
    image1= models.URLField()
    image2= models.URLField()
    image3= models.URLField()
    image4= models.URLField()
    image5= models.URLField()
    info= models.CharField(max_length=1000)
    order= models.PositiveIntegerField(null=True)
    character_id= models.ForeignKey(
        Character, 
        related_name="details", 
        on_delete=models.CASCADE,
        
    )
    class Meta:
        ordering= ["order"]
        

