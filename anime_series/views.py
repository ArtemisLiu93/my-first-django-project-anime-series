from django.shortcuts import render, redirect
from anime_series.models import AnimeSerie, Character, Character_detail
from django.contrib.auth.decorators import login_required
from anime_series.forms import Character_creation_form

# Create your views here.
def anime_list(request):
    animeseries= AnimeSerie.objects.all()
    context= {
        "animeseries": animeseries,
    }
    return render(request,"anime_series/anime.html", context)

def anime_individual(request, id):
    anime = AnimeSerie.objects.get(id=id)
    characters = Character.objects.filter(anime_id=anime)

    context = {
        'anime': anime,
        'characters': characters,
    }

    return render(request,"anime_series/individual.html", context)


@login_required
def character_info(request, id):
    details = Character.objects.get(id=id)
    characters = Character_detail.objects.filter(character_id=details)
    context= {
        'details': details,
        'characters': characters,
    }
    return render(request, "anime_series/details.html", context)


# def anime_individual(request, id):
#     anime = get_object_or_404(AnimeSerie, id)
#     context = {
#         "anime_individual":anime
#     }
#     return render(request, "anime_series/individual.html", context)

@login_required
def create_character_form(request):
    if request.method == "POST": 
        form = Character_creation_form(request.POST)
        if form.is_valid():
            character=form.save(False)
            character.user=request.user
            return redirect("")
    else:
        form = Character_creation_form()
    context ={
        "form": form,
    }
    return render(request, "anime_series/creation.html", context)
        